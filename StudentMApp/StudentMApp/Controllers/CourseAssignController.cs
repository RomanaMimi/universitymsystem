﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentMApp.BLL;
using StudentMApp.Model;

namespace StudentMApp.Controllers
{
    public class CourseAssignController : Controller
    {
        DeptManager dmanager = new DeptManager();
        TeacherManager tmanager = new TeacherManager();
        CourseManager cmanager = new CourseManager();
        CAManager caManager = new CAManager();
        
        // GET: /CourseAssign/
        public ActionResult Index()
        {
            return View();
        }

        //GET: /CourseAssign/
        public ActionResult AssignCourse()
        {
            ViewBag.deptList = dmanager.GetAllDept();
            return View();
        }

        public JsonResult CourseAssign(int deptId, int teacherId, int courseId, float courseCredit, float creditTaken, float dueCredit)
        {
            Teacher teacher = new Teacher();
            Course c = new Course();
            CourseAssign ca = new CourseAssign();

            //check if course is already assigned or not
            //var isCourseAssign = caManager.GetAllCourses().Where(a => a.CourseId == courseId && a.Courseflag == 1).ToList();
            var isCourseAssign = cmanager.GetAllCourses().Where(a => a.CId == courseId && a.CourseFlag == 1).ToList();
            if (isCourseAssign.Count > 0)
            {
                return Json(new { msg = "Course has already assigned" });
            }
            else
            {
                c.CId = courseId;
                c.DeptId = deptId;
                c.TeacherId = teacherId;
                c.CourseFlag = 1;

                //Teacher @default = tmanager.GetAllTeachers().FirstOrDefault(p => p.TId == c.TeacherId);
                //if (@default != null)
                //    c.TeacherName = @default.Name;

               
                //ca.DepartmentId = deptId;
                //ca.TeacherId = teacherId;
                //ca.CourseId = courseId;
                //Course courseDefault = cmanager.GetAllCourses().FirstOrDefault(m => m.CId == ca.CourseId);
                //if (courseDefault != null)
                //    ca.CourseSemester = courseDefault.SemName;
                //ca.Courseflag = 1;

                //after assigning course, it'll calculate the total assignCredit of a teacher 
                Teacher teacherDemo = tmanager.GetAllTeachers().FirstOrDefault(p => p.TId == teacherId);
                if (teacherDemo != null)
                {
                    float totalCredit = teacherDemo.TotalCredit + courseCredit;
                    teacher.TId = teacherId;
                    teacher.TotalCredit = totalCredit;
                }

                //assign course
                //string msg = caManager.AssignCourse(ca);
                int updateCourseStatus = cmanager.UpdateCourseStatus(c);
                int updateTeacherStatus = tmanager.UpdateTeacherStatus(teacher);

                if (updateCourseStatus > 0 && updateTeacherStatus > 0)
                {
                    string msg1 = "Assign Course Successfully";
                    
                    return Json(new { success = true, msg = msg1 }, JsonRequestBehavior.AllowGet);
                   // return Json(new { msg = "Assign Course Successfully" });
                }
                else if (cmanager.IsCourseAssign(teacherId, courseId))
                {
                    return Json(new { success = true, msg = "This course has already assigned" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, msg = "Course has not assigned" }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(true);
        }

        //get all teachers by deptId
        public JsonResult GetTeacherByDeptId(int departmentId)
        {
            var teachers = tmanager.GetAllTeachers();
            var tList = teachers.Where(a => a.DeptID == departmentId).ToList();
            return Json(tList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTeacherById(int teacherId)
        {
            var teachers = tmanager.GetAllTeachers();
            var teacher = teachers.FirstOrDefault(m => m.TId == teacherId);
            teacher.DueCredit = teacher.Credit - teacher.TotalCredit;
            return Json(teacher, JsonRequestBehavior.AllowGet);
        }

        //get all course by deptId
        public JsonResult GetCourseByDeptId(int departmentId)
        {
            var courses = cmanager.GetAllCourses();
            var cList = courses.Where(a => a.DeptId == departmentId).ToList();
            return Json(cList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCourseById(int courseId)
        {
            var courses = cmanager.GetAllCourses();
            var course = courses.FirstOrDefault(m => m.CId == courseId);
            return Json(course, JsonRequestBehavior.AllowGet);
        }
    }
}
