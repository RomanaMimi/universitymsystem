﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentMApp.BLL;
using StudentMApp.Model;

namespace StudentMApp.Controllers
{
    public class TeacherController : Controller
    {
        DeptManager dmanager = new DeptManager();
        DesignationManager desManager = new DesignationManager();
        TeacherManager manager = new TeacherManager();
        //
        // GET: /Teacher/
        public ActionResult Index()
        {
            ViewBag.TeacherList = manager.GetAllTeachers();

            if (ViewBag.TeacherList == null)
            {
                ViewBag.msg = "No data found";
                return View();
            }
            return View();
        }

        // GET: call when page load
        public ActionResult Save()
        {
            ViewBag.deptList = dmanager.GetAllDept();
            ViewBag.DesignList = desManager.GetAllInfo();
            return View();
        }

        //POST: call when button click  //TODO: check form validation
        [HttpPost]
        public ActionResult Save(Teacher teacher)
        {
            ViewBag.deptList = dmanager.GetAllDept();
            ViewBag.DesignList = desManager.GetAllInfo();
            
            teacher.TotalCredit = 0;
            if (ModelState.IsValid)
            {
                string msg = manager.SaveTeacher(teacher);
                if (msg == "Saved")
                {
                    ViewBag.Message = msg;
                }
                else
                {
                    ViewBag.ErrorMessage = msg;
                }
            }
            else
            {
                ViewBag.ErrorMessage = "Please enter all Information";
                return View();
            }  
            return View();
        }
	}
}