﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StudentMApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Science_School()
        {
            return View();
        }

        public ActionResult Engg_School()
        {
            return View();
        }

        public ActionResult BA_School()
        {
            return View();
        }

        public ActionResult ASS_School()
        {
            return View();
        }

        public ActionResult USS()
        {
            return View();
        }
    }
}