﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentMApp.BLL;
using StudentMApp.Model;

namespace StudentMApp.Controllers
{
    public class StudentController : Controller
    {

        DeptManager dmanager = new DeptManager();
        StudentManager manager = new StudentManager();

        //string RegNo = "";
        
        // GET: /Student/
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Register()
        {
            ViewBag.DeptList = dmanager.GetAllDept(); 
            return View();
        }

        [HttpPost]
        public ActionResult Register(Student student)
        {
            ViewBag.DeptList = dmanager.GetAllDept(); 

            if (ModelState.IsValid)
            {
                student.DeptCode = GetDeptCode(student.DeptId);
                student.DeptName = GetDeptName(student.DeptId);

                student.Year = GetDateYear(student.RegDate);
                student.RegNo = GenerateRegNo(student);
                //RegNo = student.RegNo;

                string testmsg = "";
                Student viewStudent = student;

                testmsg = manager.SaveStudent(student);
                if (testmsg == "Saved")
                {
                    //Todo: go to Index view page and show new student information
                    ViewBag.Message = viewStudent;
                    return View();
                }
                else
                {
                    ViewBag.ErrorMessage = testmsg;
                    return View();
                }
            }
            else
            {
                ViewBag.ErrorMessage = "Please enter all information";
                return View();
            }
            return View();
        }

        private string GetDeptName(int deptId)
        {
            List<Department> deptList = dmanager.GetAllDept();
            string DeptName = "";
            foreach (Department d in deptList)
            {
                if (d.Id == deptId)
                {
                    DeptName = d.Name;
                    break;
                }
            }
            return DeptName;
        }


        private string GetDeptCode(int deptId)
        {
            List<Department> deptList = dmanager.GetAllDept();
            string DeptCode = "";
            foreach (Department d in deptList)
            {
                if (d.Id == deptId)
                {
                    DeptCode = d.Code;
                    break;
                }
            }
            return DeptCode;
        }

        private string GetDateYear(string date)
        {
            string year = date.Substring(6, date.Length-6);
            return year;
        }

        private string GenerateRegNo(Student student)
        {

            string Zero = "";
            List<Student> sList = manager.CountStudent(student.DeptId, student.Year);
            int count = sList.Count + 1;
            int length = 3 - count.ToString().Length;

            for (int i = 0; i < length; i++)
            {
                Zero += "0";
            }

            string RegNo = student.DeptCode + "-" + student.Year + "-" + Zero + count;

            return RegNo;

        }
	}
}