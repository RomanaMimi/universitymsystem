﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentMApp.BLL;
using StudentMApp.Model;

namespace StudentMApp.Controllers
{
    public class EnrollStudentController : Controller
    {
        DeptManager dmanager = new DeptManager();
        StudentManager smanager = new StudentManager();
        CourseManager cmanager = new CourseManager();

        EnrollStudentManager manager = new EnrollStudentManager();
        int departmentId = 0;


        // GET: /EnrollStudent/
        public ActionResult Index()
        {
            return View();
        }

        Student s = new Student();
        Department d = new Department();
        Course c = new Course();


        //GET: /EnrollStudent/
        public ActionResult EnrollStudent()
        {
            ViewBag.RegNoList = smanager.GetAllRegNo();
            return View();
        }

        [HttpPost]
        // POST: /EnrollStudent/
        public ActionResult EnrollStudent(EnrollStudent enroll)
        {
            ViewBag.RegNoList = smanager.GetAllRegNo();
            if (enroll.courseId == 0)
            {
                ViewBag.ErrorMessage = "Course hasn't select yet.";
                return View();
            }

            var students = smanager.GetAllStudents();
            var student = students.FirstOrDefault(m => m.Id == enroll.RegNo);
            if (student != null)
            {
                enroll.StudentRegNo = student.RegNo;
                enroll.deptId = student.DeptId;
            }

            if (ModelState.IsValid)
            {
                //get course info
                var courses = cmanager.GetAllCourses().Where(p => p.DeptId == enroll.deptId).ToList();
                Course selectCourse = courses.FirstOrDefault(p => p.CId == enroll.courseId);
                if (selectCourse != null)
                {
                    enroll.courseName = selectCourse.CourseName;
                    enroll.courseCode = selectCourse.CourseCode;
                }

                string testmsg = manager.EnrollCourseToStudent(enroll);
                if (testmsg == "Saved")
                {
                    ViewBag.Message = testmsg;
                    return View();
                }
                else
                {
                    ViewBag.ErrorMessage = testmsg;
                    return View();
                }
            }
            else
            {
                ViewBag.ErrorMessage = "Please enter all information";
                return View();
            }
            return View();
        }



        //get student info
        public JsonResult GetStudentById(int RegNo)
        {
            var students = smanager.GetAllStudents();
            var student = students.FirstOrDefault(m => m.Id == RegNo);
           
            List<Department> deptList = dmanager.GetAllDept();
            foreach (Department d in deptList)
            {
                if (d.Id == student.DeptId)
                {
                    student.DeptName = d.Name;
                    break;
                }
            }
            return Json(student, JsonRequestBehavior.AllowGet);
        }

        //get all course by regId
        public JsonResult GetCourseByRegId(int RegNo)
        {
            var students = smanager.GetAllStudents();
            var student = students.FirstOrDefault(m => m.Id == RegNo);


            List<Department> deptList = dmanager.GetAllDept();
            foreach (Department d in deptList)
            {
                if (d.Id == student.DeptId)
                {
                    departmentId = d.Id;
                    break;
                }
            }
            var courses = cmanager.GetAllCourses();
            var cList = courses.Where(a => a.DeptId == departmentId).ToList();
            return Json(cList, JsonRequestBehavior.AllowGet);
        }
	}
}