﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentMApp.BLL;
using StudentMApp.Model;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Font = iTextSharp.text.Font;


namespace StudentMApp.Controllers
{
    public class StudentResultController : Controller
    {
        DeptManager dmanager = new DeptManager();
        StudentManager smanager = new StudentManager();
        CourseManager cmanager = new CourseManager();

        EnrollStudentManager esManager = new EnrollStudentManager();
        //StudentResultManager manager = new StudentResultManager();
        
        // GET: /StudentResult/
        public ActionResult Index()
        {
            ViewBag.RegNoList = smanager.GetAllRegNo();
            return View();
        }

        // POST: /StudentResult/
        [HttpPost]
        public ActionResult Index(StudentResult student)
        {
            ViewBag.RegNoList = smanager.GetAllRegNo();
            foreach (Student sr in ViewBag.RegNoList)
            {
                if (sr.Id == student.RegNo)
                {
                    student.StudentRegNo = sr.RegNo;
                    break;
                }
            }

            var courses = esManager.GetStudentResult();
            var cList = courses.Where(a => a.RegNo == student.RegNo).ToList();

            foreach (StudentResult course1 in cList)
            {
                if (course1.GradeLetter == "0")
                {
                    course1.GradeLetter = "Not graded yet";
                }
            }

            if (student != null && cList.Count != 0)
            {
                //generate pdf with info
                try
                {
                    Document pdfDoc = new Document(PageSize.A4, 25, 10, 25, 10);               
                    PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                    Phrase phrase = null;     
                    PdfPTable table = null;
                   
                    pdfDoc.Open();

                    //header table
                    table = new PdfPTable(2);
                    table.TotalWidth = 500f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.3f, 0.7f });
                    pdfDoc.Add(table);

                    //Name and Address
                    phrase = new Phrase();
                    phrase.Add(new Chunk("Southeast University\n\n", FontFactory.GetFont("Arial", 16, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK)));
                    phrase.Add(new Chunk("251/A and 252, Tejgaon Industrial Area\n", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK)));
                    phrase.Add(new Chunk("Dhaka-1208, Bangladesh\n", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK)));
                    pdfDoc.Add(phrase);

                    //Student info
                    PdfPTable table1 = new PdfPTable(2);
                    PdfPCell cell1 = new PdfPCell(new Phrase("Student Information"));
                    cell1.Colspan = 2;
                    cell1.HorizontalAlignment = Element.ALIGN_CENTER;
                    table1.AddCell(cell1);

                    table1.AddCell("Student Reg No. "); table1.AddCell(student.StudentRegNo);
                    table1.AddCell("Name "); table1.AddCell(student.StudentName);
                    table1.AddCell("Email "); table1.AddCell(student.StudentEmail);
                    table1.AddCell("Department "); table1.AddCell(student.Department);
                    pdfDoc.Add(table1);

                    Paragraph pText = new Paragraph("\n\n\n");
                    pdfDoc.Add(pText);

                    //Course info
                    PdfPTable table2 = new PdfPTable(3);
                    PdfPCell cell2 = new PdfPCell(new Phrase("Course Information"));
                    cell2.Colspan = 3;
                    cell2.HorizontalAlignment = Element.ALIGN_CENTER;
                    table2.AddCell(cell2);

                   //header
                    table2.AddCell("Course Code");
                    table2.AddCell("Course Name");
                    table2.AddCell("Grade");
                    
                    foreach (StudentResult test in cList)
                    {
                        table2.AddCell(test.courseCode);
                        table2.AddCell(test.courseName);
                        table2.AddCell(test.GradeLetter);
                        //count++;
                    }
                    pdfDoc.Add(table2);

                    pText = new Paragraph("\n\n");
                    pdfDoc.Add(pText);

                    pdfWriter.CloseStream = false;
                    pdfDoc.Close();

                    //create pdf
                    Response.Buffer = true;
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("content-disposition", "attachment;filename=Student_Result.pdf");
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.Write(pdfDoc);
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
            }
            return View();
        }

        public JsonResult GetStudentResult(int RegNo)
        {
            var courses = esManager.GetStudentResult();
            var cList = courses.Where(a => a.RegNo == RegNo).ToList();

            if (cList.Count == 0)
            {         
                return Json(cList);
            }

            foreach (StudentResult course1 in cList)
            {
                if (course1.GradeLetter == "0")
                {
                    course1.GradeLetter = "Not graded yet";
                }
            }
            return Json(cList, JsonRequestBehavior.AllowGet);
        }

        Student s = new Student();
        Department d = new Department();
        Course c = new Course();

        //GET: /StudentResult/
        public ActionResult StudentResult()
        {
            ViewBag.RegNoList = smanager.GetAllRegNo();
            ViewBag.GradeList = GradeList();
            return View();
        }

        [HttpPost]
        // POST: /StudentResult/
        public ActionResult StudentResult(StudentResult student)
        {
            ViewBag.RegNoList = smanager.GetAllRegNo();
            ViewBag.GradeList = GradeList();
            if (student.courseId == 0 || student.GradeLetter == "--Select--")
            {
                ViewBag.ErrorMessage = "Course/Grade hasn't selected yet";
                return View();
            }

            if (ModelState.IsValid)
            {
                var student1 = smanager.GetAllStudents().FirstOrDefault(m => m.Id == student.RegNo);
                if (student1 != null) student.deptId = student1.DeptId;

                string testmsg = esManager.SaveResultToStudent(student);
                if (testmsg == "Saved")
                {
                    ViewBag.Message = testmsg;
                    return View();
                }
                else
                {
                    ViewBag.ErrorMessage = testmsg;
                    return View();
                }
            }
            else
            {
                ViewBag.ErrorMessage = "Please enter all information";
                return View();
            }

            return View();
        }



        //get student info
        public JsonResult GetStudentById(int RegNo)
        {
            var students = smanager.GetAllStudents();
            var student = students.FirstOrDefault(m => m.Id == RegNo);
            

            List<Department> deptList = dmanager.GetAllDept();
            foreach (Department d in deptList)
            {
                if (d.Id == student.DeptId)
                {
                    student.DeptName = d.Name;
                    break;
                }
            }
            return Json(student, JsonRequestBehavior.AllowGet);
        }



        //get all course by regId
        public JsonResult GetCourseByRegId(int RegNo)
        {
            var courses = esManager.GetStudentCourse(RegNo);
            return Json(courses, JsonRequestBehavior.AllowGet);
        }

        private List<Grade> GradeList()
        {
            List<Grade> GradeList = new List<Grade>()
            {
                new Grade{Id = 0, GradeName = "Grades"},
                new Grade{Id = 1, GradeName = "A+"},
                new Grade{Id = 2, GradeName = "A"},
                new Grade{Id = 3, GradeName = "A-"},
                new Grade{Id = 4, GradeName = "B+"},
                new Grade{Id = 5, GradeName = "B"},
                new Grade{Id = 6, GradeName = "B-"},
                new Grade{Id = 7, GradeName = "C+"},
                new Grade{Id = 8, GradeName = "C"},
                new Grade{Id = 9, GradeName = "C-"},
                new Grade{Id = 10, GradeName = "D+"},
                new Grade{Id = 11, GradeName = "D"},
                new Grade{Id = 12, GradeName = "D-"},
                new Grade{Id = 13, GradeName = "F"}
            };

            return GradeList;
        }


	}
}