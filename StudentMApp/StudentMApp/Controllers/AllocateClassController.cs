﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentMApp.BLL;
using StudentMApp.Model;

namespace StudentMApp.Controllers
{
    public class AllocateClassController : Controller
    {
        DeptManager dmanager = new DeptManager();
        RoomManager rmanager = new RoomManager(); 
        CourseManager cmanager = new CourseManager();

        AllocateClassManager manager = new AllocateClassManager();
        ClassScheduleManager smanager = new ClassScheduleManager();
        //
        // GET: /view Class schedule/
        public ActionResult Index()
        {
            ViewBag.deptList = dmanager.GetAllDept();
            return View();
        }

        [HttpPost]
        // POST: /view Class schedule/
        public ActionResult Index(int departmentId)
        {
            ViewBag.deptList = dmanager.GetAllDept();
            return View();
        }

        public JsonResult GetDeptCourse(int departmentId)
        {
            var scheduleCourse = smanager.GetScheduleCourses();
            var courses = cmanager.GetAllCourses();
            List<Course> cList = courses.Where(a => a.DeptId == departmentId).ToList();

            foreach (Course course1 in cList)
            {
                var sCourse = scheduleCourse.Where(a => a.CourseId == course1.CId).ToList();

                if (sCourse.Count == 0 || course1.Classflag == 0)
                {
                    course1.Schedule = "Not scheduled yet";
                }
                if (sCourse.Count == 1 && course1.Classflag == 1)
                {
                    foreach (var c in sCourse)
                    {
                        course1.Schedule = c.Schedule;
                    }
                }
                if (sCourse.Count > 1 && course1.Classflag == 1)
                {
                    foreach (var c in sCourse)
                    {
                        course1.Schedule += c.Schedule + "\n\n";
                    }
                }
            }
            return Json(cList, JsonRequestBehavior.AllowGet);
        }


        AllocateClass aC = new AllocateClass();
        ClassSchedule schedule = new ClassSchedule();
        Course c1 = new Course();

        //Get
        public ActionResult AllocateClassroom()
        {
            ViewBag.deptList = dmanager.GetAllDept();
            ViewBag.roomList = rmanager.GetRooms();
            ViewBag.dayList = DayList();
            return View();
        }

        //post
        [HttpPost]
        public ActionResult AllocateClassroom(AllocateClass aclass)
        {
            ViewBag.deptList = dmanager.GetAllDept();
            ViewBag.roomList = rmanager.GetRooms();
            ViewBag.dayList = DayList();

                if (ModelState.IsValid)
                {
                    //calculate start time
                    var Time1 = aclass.StartTime;
                    var hour = Convert.ToInt32(Time1.Substring(0, 2));
                    var minute = Convert.ToInt32(Time1.Substring(3, 2));
                    var sFormat = Time1.Substring(6, 2);
                    TimeSpan startTime = new TimeSpan(hour, minute, 0);
                    
                    //calculate end time
                    var Time2 = aclass.EndTime;
                    var hour1 = Convert.ToInt32(Time2.Substring(0, 2));
                    var minute1 = Convert.ToInt32(Time2.Substring(3, 2));
                    var eFormat = Time2.Substring(6, 2);
                    TimeSpan endTime = new TimeSpan(hour1, minute1, 0);

                    if (startTime == endTime)
                    {
                        ViewBag.ErrorMessage = "Invalid Time";
                        return View();
                    }
                    else
                    {
                        aclass.StartTime = Time1.Substring(0, 2) + ":" + Time1.Substring(3, 2);
                        aclass.EndTime = Time2.Substring(0, 2) + ":" + Time2.Substring(3, 2);
                        aclass.StartTimeFormat = sFormat;
                        aclass.EndTimeFormat = eFormat;

                        //for Allocate classrom table
                        aC.DeptId = aclass.DeptId;
                        aC.CourseId = aclass.CourseId;
                        aC.RoomNo = aclass.RoomNo;
                        aC.DayName = aclass.DayName;
                        aC.StartTime = aclass.StartTime + " " + aclass.StartTimeFormat;
                        aC.StartTimeFormat = aclass.StartTimeFormat;
                        aC.EndTime = aclass.EndTime + " " + aclass.EndTimeFormat;
                        aC.EndTimeFormat = aclass.EndTimeFormat;

                        //for schedule table
                        var course = cmanager.GetAllCourses().FirstOrDefault(m => m.CId == aclass.CourseId);
                        schedule.CourseId = aclass.CourseId;
                        schedule.Schedule = "R. No: " + aclass.RoomNo + ", " + aclass.DayName + ", " + aC.StartTime + " - " + aC.EndTime;
                        schedule.ClassRoomFlag = 1;
                        
                        if (course != null)
                        {
                            //schedule.CourseCode = course.CourseCode;
                            //schedule.CourseName = course.CourseName;

                            //for course table
                            c1.CId = course.CId;
                            c1.DeptId = course.DeptId;
                            c1.Classflag = 1;
                        }
                       
                        string msgTxt = manager.AllocateClass(aC);
                        if (msgTxt == "saved" && smanager.SaveSchedule(schedule) > 0 && cmanager.UpdateClassStatus(c1) > 0)
                        {
                            ViewBag.Message = "Allocation Successful";
                        }
                        else
                        {
                            ViewBag.ErrorMessage = msgTxt;
                        }
                    }
                }
                else
                {
                    ViewBag.ErrorMessage = "Room allocation fail";
                    return View();
                }

            return View();  
        }

        public JsonResult GetCourseByDeptId(int departmentId)
        {
            var courses = cmanager.GetAllCourses();
            var cList = courses.Where(a => a.DeptId == departmentId).ToList();
            return Json(cList, JsonRequestBehavior.AllowGet);
        }

        // GET: /Unallocate Classroom/
        public ActionResult UnalloClassroom()
        {
            //Todo: create confirmation box 
            return View();
        }

        public JsonResult Unallocate(bool flag)
        {
            int row1 = 0, row2 = 0, row3 = 0;

            if (flag)
            {
                //allocate class
                var classList = manager.GetAllocateClasses().Where(p => p.Classflag != 0).ToList();
                foreach (AllocateClass ac in classList)
                {
                    ac.Classflag = 0;
                    row1 = manager.UnalloClassroom(ac);
                }

                //schedule
                var sList = smanager.GetScheduleCourses().Where(p => p.ClassRoomFlag == 1).ToList();
                foreach (ClassSchedule assign in sList)
                {
                    assign.ClassRoomFlag = 0;
                    assign.Schedule = string.Empty;
                    row2 = smanager.UpdateSchedule(assign);
                }

                //course
                var courseList = cmanager.GetAllCourses().Where(p => p.Classflag == 1).ToList();
                foreach (Course course in courseList)
                {
                    course.Classflag = 0;
                    row3 = cmanager.UpdateClassFlagStatus(course);
                }

                if (row1 > 0 && row2 > 0 && row3 > 0)
                {
                    return Json(new { success = true, msg = "Successfully unallocate all classroom" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, msg = "Already unallocated all classroom" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                 return Json(new { success = true, msg = "Failed to unallocate classroom" }, JsonRequestBehavior.AllowGet);
            }
            return Json(true);
        }

        private List<Day> DayList()
        {
            List<Day> DayList = new List<Day>()
            {
                new Day{Id = 0, DayName = "Days"},
                new Day{Id = 1, DayName = "Sat"},
                new Day{Id = 2, DayName = "Sun"},
                new Day{Id = 3, DayName = "Mon"},
                new Day{Id = 4, DayName = "Tue"},
                new Day{Id = 5, DayName = "Wed"},
                new Day{Id = 6, DayName = "Thurs"},
                new Day{Id = 6, DayName = "Fri"} 
            };
            return DayList;
        }
	}
}