﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentMApp.BLL;
using StudentMApp.Model;

namespace StudentMApp.Controllers
{
    public class DeptController : Controller
    {
        DeptManager manager = new DeptManager();
        //
        // GET: /Dept
        public ActionResult Index()
        {
            ViewBag.Total = manager.GetAllDept().Count;
            if (manager.GetAllDept() == null)
            {
                ViewBag.msg = "No Data found";
                return View();
            }
            ViewBag.DeptList = manager.GetAllDept();
            return View();
        }

        // GET: call when page load
        public ActionResult Save()
        {
            return View();
        }

        //POST: call when button click
        [HttpPost]
        public ActionResult Save(Department dept)
        {
            if (ModelState.IsValid)
            {
                string msg = manager.Save(dept);

                if (msg == "Saved")
                {
                    ViewBag.Message = msg;
                }
                else
                {
                    ViewBag.ErrorMessage = msg;
                }
            } 
            return View();
        }
    }
}