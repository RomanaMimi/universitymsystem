﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentMApp.BLL;
using StudentMApp.Model;

namespace StudentMApp.Controllers
{
    public class CourseController : Controller
    {
        DeptManager dmanager = new DeptManager();
        SemesterManager sManager = new SemesterManager();

        CourseManager manager = new CourseManager();
        CAManager caManager = new CAManager();
        TeacherManager tManager = new TeacherManager();

        //
        // GET: /Course/
        public ActionResult Index()
        {
            //Todo: view course statics
            ViewBag.deptList = dmanager.GetAllDept();
            return View();
        }

        [HttpPost]
        public ActionResult Index(int departmentId)
        {
            //Todo: view course statics
            ViewBag.deptList = dmanager.GetAllDept();
            return View();
        }

        //Todo: view course statics
        public JsonResult GetDeptCourse(int departmentId)
        {
            var courses = manager.GetAllCourses();
            var cList = courses.Where(a => a.DeptId == departmentId).ToList();

            foreach (Course course1 in cList)
            {
                if (course1.CourseFlag == 0)
                {
                    course1.TeacherName = "Not assigned yet";
                }
            }
            return Json(cList, JsonRequestBehavior.AllowGet);
        }

        // GET: call when page load
        public ActionResult Save()
        {
            ViewBag.deptList = dmanager.GetAllDept();
            ViewBag.semList = sManager.GetAllInfo();
            return View();
        }

        //POST: call when button click 
        [HttpPost]
        public ActionResult Save(Course crs)
        {
            ViewBag.deptList = dmanager.GetAllDept();
            ViewBag.semList = sManager.GetAllInfo();
            List<Semester> sList = ViewBag.semList;

                if (ModelState.IsValid)
                {
                    ////addition: adding semester name
                    //if (crs.SemesterId != 0)
                    //{
                    //    Semester semester = sList.FirstOrDefault(p => p.SId == crs.SemesterId);
                    //    if (semester != null)
                    //        crs.SemName = semester.SName;
                    //}
                    ////end

                    
                    crs.TeacherId = 0;
                    //crs.TeacherName = ""; // addition: teacher's name
                    crs.CourseFlag = 0;
                    crs.Classflag = 0;

                    string msg = manager.SaveCourse(crs);
                    if (msg == "Saved")
                    {
                        ViewBag.Message = msg;
                    }
                    else
                    {
                        ViewBag.ErrorMessage = msg;
                    }
                }
                else
                {
                    ViewBag.ErrorMessage = "Please enter all information";
                    return View();
                }
            return View();
        }


        // GET: /Unassign Course/
        public ActionResult UnassignCourse()
        {
            //Todo: create confirmation box 
            return View();
        }

        public JsonResult Unassign(bool flag)
        {
            int row1 = 0, row3 = 0;

            if (flag)
            {
                var teacherList = tManager.GetAllTeachers().Where(p => p.TotalCredit > 0).ToList();
                foreach (Teacher teacher in teacherList)
                {
                    teacher.TotalCredit = 0;
                    row1 = tManager.UpdateTeacherStatus(teacher);                
                }

                //var courseAssignList = caManager.GetAllCourses().Where(p => p.Courseflag == 1).ToList();
                //foreach (CourseAssign assign in courseAssignList)
                //{
                //    assign.Courseflag = 0;
                //    row2 = caManager.UpdateCourseStatus(assign);
                //}

                var courseList = manager.GetAllCourses().Where(p => p.CourseFlag == 1).ToList();
                foreach (Course course in courseList)
                {
                    course.CourseFlag = 0;
                    row3 = manager.UpdateCourseFlagStatus(course);
                }

                if (row1 > 0 && row3 > 0)
                {
                    return Json(new { msg = "Successfully unassign all courses" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { msg = "Already unassign all courses" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { msg = "Failed to unassign" }, JsonRequestBehavior.AllowGet);
            }
            return Json(true);
        }
	}
}