﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using StudentMApp.Model;

namespace StudentMApp.Gateway
{
    public class DeptGateway
    {
        public string ConnectionString = WebConfigurationManager.ConnectionStrings["SMString"].ConnectionString;

        public int Save(Department dept)
        {

            SqlConnection connection = new SqlConnection(ConnectionString);

            string query = "INSERT INTO Dept(Code, Name) VALUES('" + dept.Code + "', '" + dept.Name + "')";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();
            int rowAffected = command.ExecuteNonQuery();
            connection.Close();
            return rowAffected;
        }

        public bool IsExist(string code, string name)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string query = "SELECT * FROM Dept WHERE Code='" + code + "' OR Name='"+name+"'";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();

            SqlDataReader reader = command.ExecuteReader();
            bool isExist = reader.HasRows;
            reader.Close();
            connection.Close();
            return isExist;

        }

        //TOdo: must add GetAllReport() code
        public List<Department> GetAllDept()
        {
            List<Department> aList = new List<Department>();
            SqlConnection connection = new SqlConnection(ConnectionString);

            string query = "SELECT * FROM Dept";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Department dept1 = new Department();
                    dept1.Id = Convert.ToInt32(reader["Id"]);
                    dept1.Code = reader["Code"].ToString();
                    dept1.Name = reader["Name"].ToString();

                    aList.Add(dept1);
                }
            }
            reader.Close();
            connection.Close();
            return aList;
        }

    }
}