﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using StudentMApp.Model;

namespace StudentMApp.Gateway
{
    public class RoomGateway
    {
        public string ConnectionString = WebConfigurationManager.ConnectionStrings["SMString"].ConnectionString;

        public List<Room> GetRooms()
        {
            List<Room> roomList = new List<Room>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            //query
            string query = "SELECT * FROM Room";
            //execute
            SqlCommand command = new SqlCommand(query, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Room sm = new Room();
                    sm.Id = Convert.ToInt32(reader["Id"]);
                    sm.RoomNo = reader["RoomNo"].ToString();
                    roomList.Add(sm);

                }
            }
            reader.Close();
            connection.Close();

            return roomList;
        } 
    }
}