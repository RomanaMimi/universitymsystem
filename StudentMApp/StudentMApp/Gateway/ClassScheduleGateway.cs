﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using StudentMApp.BLL;
using StudentMApp.Model;

namespace StudentMApp.Gateway
{
    public class ClassScheduleGateway
    {
        public string ConnectionString = WebConfigurationManager.ConnectionStrings["SMString"].ConnectionString;

        CourseManager cManager = new CourseManager();

        public int SaveSchedule(ClassSchedule ac)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string query = "INSERT INTO ClassSchedule(CourseId, Schedule, ClassFlag) VALUES('" + ac.CourseId + "', '" + ac.Schedule + "', '" + ac.ClassRoomFlag + "')";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();
            int row = command.ExecuteNonQuery();
            connection.Close();
            return row;
        }

        public int UpdateSchedule(ClassSchedule ac)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string query = "UPDATE ClassSchedule SET Schedule = '" + ac.Schedule + "', ClassFlag='"+ac.ClassRoomFlag+"' WHERE CourseId ='"+ac.CourseId+"'";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();
            int row = command.ExecuteNonQuery();
            connection.Close();
            return row;
        }

        public int UpdateClassFlag(ClassSchedule ac)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string query = "UPDATE ClassSchedule SET ClassFlag='" + ac.ClassRoomFlag + "',  WHERE CourseId ='" + ac.CourseId + "'";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();
            int row = command.ExecuteNonQuery();
            connection.Close();
            return row;
        }

        public List<ClassSchedule> GetScheduleCourses()
        {
            List<ClassSchedule> cList = new List<ClassSchedule>();
            SqlConnection connection = new SqlConnection(ConnectionString);

            string query = "SELECT * FROM ClassSchedule";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    ClassSchedule c = new ClassSchedule();
                    c.CourseId = Convert.ToInt32(reader["CourseId"]);
                    Course course = cManager.GetAllCourses().FirstOrDefault(p=>p.CId == c.CourseId);
                    if (course != null)
                    {
                         c.CourseCode = course.CourseCode;
                         c.CourseName = course.CourseName;
                    }
                   
                    c.Schedule = reader["Schedule"].ToString();
                    c.ClassRoomFlag = Convert.ToInt32(reader["ClassFlag"]);
                    cList.Add(c);
                }
            }
            reader.Close();
            connection.Close();

            return cList;
        }

    }
}