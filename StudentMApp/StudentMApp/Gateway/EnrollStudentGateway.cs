﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using StudentMApp.Model;

namespace StudentMApp.Gateway
{
    public class EnrollStudentGateway
    {
        public string ConnectionString = WebConfigurationManager.ConnectionStrings["SMString"].ConnectionString;

        public bool IsExist(EnrollStudent enroll)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string query = "SELECT * FROM EnrollStudent WHERE StudentId='" + enroll.RegNo + "' AND CourseId='" + enroll.courseId + "'";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();

            SqlDataReader reader = command.ExecuteReader();
            bool isExist = reader.HasRows;
            reader.Close();
            connection.Close();
            return isExist;
        }



        public int EnrollCourseToStudent(EnrollStudent enroll)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string query = "INSERT INTO EnrollStudent(StudentId, StudentRegNo, DeptId, CourseId, CourseCode, CourseName, EnrollDate, Grade) VALUES('" + enroll.RegNo + "', '"+enroll.StudentRegNo+"', '" + enroll.deptId + "', '" + enroll.courseId + "', '" + enroll.courseCode + "', '" + enroll.courseName + "' , '" + enroll.EnrollDate + "', '0')";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();
            int rowAffected = command.ExecuteNonQuery();
            connection.Close();
            return rowAffected;
        }



        public List<Course> GetStudentCourse(int studentId)
        {
            List<Course> aList = new List<Course>();
            SqlConnection connection = new SqlConnection(ConnectionString);

            string query = "SELECT CourseId, CourseCode ,CourseName FROM EnrollStudent WHERE StudentId='" + studentId + "'";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Course c = new Course();
                    c.CId = Convert.ToInt32(reader["CourseId"]);
                    c.CourseCode = reader["CourseCode"].ToString();
                    c.CourseName = reader["CourseName"].ToString();
                    // s.DeptId = Convert.ToInt32(reader["DeptId"]);
                    //  s.RegDate = reader["RegDate"].ToString();

                    aList.Add(c);
                }
            }
            reader.Close();
            connection.Close();
            return aList;
        }



        public bool IsResultExist(StudentResult student)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            bool isExist = false;
            string query = "SELECT Grade FROM EnrollStudent WHERE StudentId='" + student.RegNo + "' AND CourseId='" + student.courseId + "'";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();

            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Grade g = new Grade();
                    g.GradeName = reader["Grade"].ToString();
                    if (g.GradeName == "0")
                    {
                        isExist = false;
                    }
                    else
                    {
                        isExist = true;
                    }
                }
            }

            reader.Close();
            connection.Close();
            return isExist;
        }




        public int SaveResultToStudent(StudentResult student)
        {
            //Connection
            SqlConnection connection = new SqlConnection(ConnectionString);
            //Query
            string Query = "UPDATE EnrollStudent SET Grade='" + student.GradeLetter + "' WHERE StudentId='" + student.RegNo + "' AND CourseId='" + student.courseId + "'";
            //Execute
            SqlCommand command = new SqlCommand(Query, connection);
            connection.Open();
            int rowAffected = command.ExecuteNonQuery();
            return rowAffected;

        }




        public List<StudentResult> GetStudentResult()
        {
            List<StudentResult> aList = new List<StudentResult>();
            SqlConnection connection = new SqlConnection(ConnectionString);

            string query = "SELECT StudentId, CourseId, CourseCode ,CourseName, Grade FROM EnrollStudent";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    StudentResult c = new StudentResult();
                    c.RegNo = Convert.ToInt32(reader["StudentId"]);
                    c.courseId = Convert.ToInt32(reader["CourseId"]);
                    c.courseCode = reader["CourseCode"].ToString();
                    c.courseName = reader["CourseName"].ToString();
                    c.GradeLetter = reader["Grade"].ToString();
                    // s.DeptId = Convert.ToInt32(reader["DeptId"]);
                    //  s.RegDate = reader["RegDate"].ToString();

                    aList.Add(c);
                }
            }
            reader.Close();
            connection.Close();
            return aList;
        }
    }
}