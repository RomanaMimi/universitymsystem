﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using StudentMApp.BLL;
using StudentMApp.Model;

namespace StudentMApp.Gateway
{
    public class TeacherGateway
    {
        public string ConnectionString = WebConfigurationManager.ConnectionStrings["SMString"].ConnectionString;
        DeptManager dmanager = new DeptManager();
        DesignationManager desManager = new DesignationManager();
        
        public int SaveTeacher(Teacher tea)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string query = "INSERT INTO Teacher(Name, Address, Email, ContactNo, DesignationID, Credit, AssignCredit, DeptID) VALUES('" + tea.Name + "', '" + tea.Address + "', '" + tea.Email + "', '" + tea.Contact + "', '" + tea.DesignID + "', '" + tea.Credit + "', '" + tea.TotalCredit + "', '"+tea.DeptID+"')";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();
            int row = command.ExecuteNonQuery();  
            connection.Close();
            return row;
        }

        public int UpdateTeacherStatus(Teacher t)
        {
            //Connection
            SqlConnection connection = new SqlConnection(ConnectionString);
            //Query
            string Query = "UPDATE Teacher SET AssignCredit='" + t.TotalCredit + "' WHERE Id='" + t.TId + "'";
            //Execute
            SqlCommand command = new SqlCommand(Query, connection);
            connection.Open();
            int rowAffected = command.ExecuteNonQuery();
            return rowAffected;
        }

        public bool IsExist(string email)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string query = "SELECT * FROM Teacher WHERE Email='" + email + "'";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();

            SqlDataReader reader = command.ExecuteReader();
            bool isExist = reader.HasRows;
            reader.Close();
            connection.Close();
            return isExist;
        }

        public List<Teacher> GetAllTeachers()
        {
            List<Teacher> tList = new List<Teacher>();
            SqlConnection connection = new SqlConnection(ConnectionString);

            string query = "SELECT * FROM Teacher";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Teacher teacher = new Teacher();
                    teacher.TId = Convert.ToInt32(reader["Id"]);
                    teacher.Name = reader["Name"].ToString();
                    teacher.Address = reader["Address"].ToString();
                    teacher.Email = reader["Email"].ToString();
                    teacher.Contact = reader["ContactNo"].ToString();
                    teacher.DesignID = Convert.ToInt32(reader["DesignationID"]);
                    Designation designation = desManager.GetAllInfo().FirstOrDefault(n => n.Id == teacher.DesignID);
                    if (designation != null)
                        teacher.Designation = designation.Name;

                    teacher.Credit = (float)Convert.ToDouble(reader["Credit"]);
                    teacher.TotalCredit = (float)Convert.ToDouble(reader["AssignCredit"]);
                    teacher.DeptID = Convert.ToInt32(reader["DeptID"]);
                    Department department = dmanager.GetAllDept().FirstOrDefault(p => p.Id == teacher.DeptID);
                    if (department != null)
                        teacher.Department = department.Code;

                    tList.Add(teacher);
                }
            }
            reader.Close();
            connection.Close();

            return tList;
        }
    }
}
