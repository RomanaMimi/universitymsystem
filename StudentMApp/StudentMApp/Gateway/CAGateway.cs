﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using StudentMApp.Model;

namespace StudentMApp.Gateway
{
    public class CAGateway
    {
        public string ConnectionString = WebConfigurationManager.ConnectionStrings["SMString"].ConnectionString;

        public int AssignCourse(CourseAssign ca)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            //assign course to teacher
            string query = "INSERT INTO CourseAssign(DeptId, TeacherId, CourseId, CourseSem, Courseflag) VALUES('" + ca.DepartmentId + "', '" + ca.TeacherId + "', '"+ca.CourseId+"', '"+ca.CourseSemester+"', '"+ca.Courseflag+"')";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();
            int row = command.ExecuteNonQuery();
            connection.Close();
            return row;
        }

        public int UpdateCourseStatus(CourseAssign ca)
        {
            //Connection
            SqlConnection connection = new SqlConnection(ConnectionString);
            //Query
            string Query = "UPDATE CourseAssign SET CourseFlag='" + ca.Courseflag + "' WHERE Id='" + ca.Id + "'";
            //Execute
            SqlCommand command = new SqlCommand(Query, connection);
            connection.Open();
            int rowAffected = command.ExecuteNonQuery();
            return rowAffected;

        }
       

        public List<CourseAssign> GetAllCourses()
        {
            List<CourseAssign> cList = new List<CourseAssign>();
            SqlConnection connection = new SqlConnection(ConnectionString);

            string query = "SELECT * FROM CourseAssign";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    CourseAssign c = new CourseAssign();
                    c.Id = Convert.ToInt32(reader["Id"]);
                    c.DepartmentId = Convert.ToInt32(reader["DeptId"]);
                    c.TeacherId = Convert.ToInt32(reader["TeacherId"]);
                    c.CourseId = Convert.ToInt32(reader["CourseId"]);
                    c.CourseSemester = reader["CourseSem"].ToString();
                    c.Courseflag = Convert.ToInt32(reader["Courseflag"]);
                    cList.Add(c);
                }
            }
            reader.Close();
            connection.Close();

            return cList;
        }

    }
}