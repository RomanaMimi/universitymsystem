﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using StudentMApp.Model;

namespace StudentMApp.Gateway
{
    public class StudentGateway
    {
        public string ConnectionString = WebConfigurationManager.ConnectionStrings["SMString"].ConnectionString;


        public List<Student> GetAllRegNo()
        {
            List<Student> aList = new List<Student>();
            SqlConnection connection = new SqlConnection(ConnectionString);

            string query = "SELECT Id, RegNo FROM Student";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Student s = new Student();
                    s.Id = Convert.ToInt32(reader["Id"]);
                    s.RegNo = reader["RegNo"].ToString();
                    aList.Add(s);
                }
            }
            reader.Close();
            connection.Close();
            return aList;
        }





        public bool IsExist(string email)
	{
		SqlConnection connection = new SqlConnection(ConnectionString);

            string query = "SELECT * FROM Student WHERE Email='" + email + "'";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();

            SqlDataReader reader = command.ExecuteReader();
            bool isExist = reader.HasRows;
            reader.Close();
            connection.Close();
            return isExist;
	}



	string NewRegNo = ""; string DeptName ="";
	public int SaveStudent(Student s)
	{
		 SqlConnection connection = new SqlConnection(ConnectionString);
	    
	    NewRegNo = s.RegNo;
	    DeptName = s.DeptName;

            string query = "INSERT INTO Student(RegNo, Name, Email, ContactNo, Address, RegDate, DeptId) VALUES('" + s.RegNo + "', '" + s.Name + "', '"+s.Email+"', '"+s.ContactNo+"', '"+s.Address+"', '"+s.RegDate+"', '"+s.DeptId+"')";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();
            int rowAffected = command.ExecuteNonQuery();
            connection.Close();
            return rowAffected;
	}




	public Student GetNewStudent(string NewRegNo)
	{
	    Student s = new Student();
            SqlConnection connection = new SqlConnection(ConnectionString);

            string query = "SELECT * FROM Student WHERE RegNo='" + NewRegNo + "'";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
		    s.Id = Convert.ToInt32(reader["Id"]);
		    s.RegNo = reader["RegNo"].ToString();
		    s.Name = reader["Name"].ToString();
	 	    s.Email = reader["Email"].ToString();
	 	    s.ContactNo = reader["ContactNo"].ToString();
		    s.Address = reader["Address"].ToString();
		    s.RegDate = reader["RegDate"].ToString();
            s.DeptName = DeptName;
            //s.RegDate = reader["Date"].ToString();
                }
            }
            reader.Close();
            connection.Close();
            return s;
	}




	 public List<Student> CountStudent(int deptId, string year)
        {
            List<Student> aList = new List<Student>();
            SqlConnection connection = new SqlConnection(ConnectionString);

            string query = "SELECT * FROM Student WHERE DeptId='" + deptId + "' AND RegDate LIKE'%" + year + "'";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Student s = new Student();
		            s.Id = Convert.ToInt32(reader["Id"]);
		            s.RegNo = reader["RegNo"].ToString();
                    s.DeptId = Convert.ToInt32(reader["DeptId"]);
                    s.RegDate = reader["RegDate"].ToString();

                    aList.Add(s);
                }
            }
            reader.Close();
            connection.Close();
            return aList;
        }




     public List<Student> GetAllStudents()
     {
         List<Student> aList = new List<Student>();
         SqlConnection connection = new SqlConnection(ConnectionString);

         string query = "SELECT * FROM Student";
         SqlCommand command = new SqlCommand(query, connection);

         connection.Open();
         SqlDataReader reader = command.ExecuteReader();
         if (reader.HasRows)
         {
             while (reader.Read())
             {
                 Student s = new Student();
                 s.Id = Convert.ToInt32(reader["Id"]);
                 s.RegNo = reader["RegNo"].ToString();
                 s.Name = reader["Name"].ToString();
                 s.Email = reader["Email"].ToString();
                 s.ContactNo = reader["ContactNo"].ToString();
                 s.Address = reader["Address"].ToString();
                 s.RegDate = reader["RegDate"].ToString();
                 s.DeptId = Convert.ToInt32(reader["DeptId"]); ;

                 aList.Add(s);
             }
         }
         reader.Close();
         connection.Close();
         return aList;
     }

    }
}