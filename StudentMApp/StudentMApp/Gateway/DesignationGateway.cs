﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using StudentMApp.Model;

namespace StudentMApp.Gateway
{
    public class DesignationGateway
    {
        public string ConnectionString = WebConfigurationManager.ConnectionStrings["SMString"].ConnectionString;

        public List<Designation> GetAllInfo()
        {
            List<Designation> dList = new List<Designation>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            //query
            string query = "SELECT * FROM Designation";
            //execute
            SqlCommand command = new SqlCommand(query, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Designation ds = new Designation();
                    ds.Id = Convert.ToInt32(reader["Id"]);
                    ds.Name = reader["Name"].ToString();
                    dList.Add(ds);

                }
            }
            reader.Close();
            connection.Close();

            return dList;
        } 
    }
}