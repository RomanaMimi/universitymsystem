﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Configuration;
using StudentMApp.BLL;
using StudentMApp.Model;

namespace StudentMApp.Gateway
{
    public class CourseGateway
    {
        public string ConnectionString = WebConfigurationManager.ConnectionStrings["SMString"].ConnectionString;

        SemesterManager sManager = new SemesterManager();
        TeacherManager tManager = new TeacherManager();

        public int SaveCourse(Course crs)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string query = "INSERT INTO Course(Code, Name, Credit, Des, DeptId, SemId, TeacherId, CourseFlag, ClassFlag) VALUES('" + crs.CourseCode + "', '" + crs.CourseName + "', '" + crs.CourseCredit + "', '" + crs.CourseDes + "', '" + crs.DeptId + "', '" + crs.SemesterId + "', '" + crs.TeacherId + "', '" + crs.CourseFlag + "', '"+crs.Classflag+"')";
            SqlCommand command1 = new SqlCommand(query, connection);

            connection.Open();
            int row = command1.ExecuteNonQuery();
            connection.Close();
            return row;
        }

        public bool IsExist(string code, string name)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string query = "SELECT * FROM Course WHERE Code='" + code + "' OR Name='" + name + "'";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();

            SqlDataReader reader = command.ExecuteReader();
            bool isExist = reader.HasRows;
            reader.Close();
            connection.Close();
            return isExist;
        }

        public bool IsCourseAssign(int teacherId, int courseId)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string query = "SELECT * FROM Course WHERE Id='" + courseId + "' AND TeacherId='" + teacherId + "' AND CourseFlag=1";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();

            SqlDataReader reader = command.ExecuteReader();
            bool isExist = reader.HasRows;
            reader.Close();
            connection.Close();
            return isExist;

        }

        public List<Course> GetAllCourses()
        {
            List<Course> cList = new List<Course>();
            SqlConnection connection = new SqlConnection(ConnectionString);

            string query = "SELECT * FROM Course";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Course c = new Course();
                    c.CId = Convert.ToInt32(reader["Id"]);
                    c.CourseCode = reader["Code"].ToString();
                    c.CourseName = reader["Name"].ToString();
                    c.CourseCredit = (float)Convert.ToDouble(reader["Credit"]);
                    c.CourseDes = reader["Des"].ToString();
                    c.DeptId = Convert.ToInt32(reader["DeptId"]);
                    c.SemesterId = Convert.ToInt32(reader["SemId"]);
                    Semester semester = sManager.GetAllInfo().FirstOrDefault(p=>p.SId == c.SemesterId);
                    if (semester != null)
                        c.SemName = semester.SName;

                    c.TeacherId = Convert.ToInt32(reader["TeacherId"]);
                    Teacher teacher = tManager.GetAllTeachers().FirstOrDefault(p=>p.TId == c.TeacherId);
                    if (teacher != null)
                        c.TeacherName = teacher.Name;

                    c.CourseFlag = Convert.ToInt32(reader["CourseFlag"]);
                    c.Classflag = Convert.ToInt32(reader["ClassFlag"]);
                    cList.Add(c);
                }
            }
            reader.Close();
            connection.Close();

            return cList;
        }



        public int UpdateCourseStatus(Course c)
        {
            //Connection
            SqlConnection connection = new SqlConnection(ConnectionString);
            //Query
            string Query = "UPDATE Course SET TeacherId='" + c.TeacherId + "',CourseFlag='" + c.CourseFlag + "' WHERE Id='"+c.CId+"' AND DeptId='" + c.DeptId + "'";
            //Execute
            SqlCommand command = new SqlCommand(Query, connection);
            connection.Open();
            int rowAffected = command.ExecuteNonQuery();
            return rowAffected;

        }



        public int UpdateClassStatus(Course c)
        {
            //Connection
            SqlConnection connection = new SqlConnection(ConnectionString);
            //Query
            string Query = "UPDATE Course SET ClassFlag='" + c.Classflag + "' WHERE Id='" + c.CId + "' AND DeptId='" + c.DeptId + "'";
            //Execute
            SqlCommand command = new SqlCommand(Query, connection);
            connection.Open();
            int rowAffected = command.ExecuteNonQuery();
            return rowAffected;

        }

        public int UpdateCourseFlagStatus(Course c)
        {
            //Connection
            SqlConnection connection = new SqlConnection(ConnectionString);
            //Query
            string Query = "UPDATE Course SET CourseFlag='" + c.CourseFlag + "' WHERE Id='" + c.CId + "'";
            //Execute
            SqlCommand command = new SqlCommand(Query, connection);
            connection.Open();
            int rowAffected = command.ExecuteNonQuery();
            return rowAffected;
        }

        public int UpdateClassFlagStatus(Course c)
        {
            //Connection
            SqlConnection connection = new SqlConnection(ConnectionString);
            //Query
            string Query = "UPDATE Course SET ClassFlag='" + c.Classflag + "' WHERE Id='" + c.CId + "'";
            //Execute
            SqlCommand command = new SqlCommand(Query, connection);
            connection.Open();
            int rowAffected = command.ExecuteNonQuery();
            return rowAffected;

        }



        public int UnassignCourses(Course course)
        {
            //Connection
            SqlConnection connection = new SqlConnection(ConnectionString);
            //Query
            string Query = "UPDATE CourseAssign SET Courseflag='" + course.CourseFlag + "'";
            //Execute
            SqlCommand command = new SqlCommand(Query, connection);
            connection.Open();
            int rowAffected = command.ExecuteNonQuery();
            int row = Unassign(course);

            return rowAffected+row ;

        }


        private int Unassign(Course course)
        {
            //Connection
            SqlConnection connection = new SqlConnection(ConnectionString);
            //Query
            string Query = "UPDATE Course SET TeacherId='" + course.TeacherId + "', TeacherName='"+course.TeacherName+"', Courseflag='" + course.CourseFlag + "'";
            //Execute
            SqlCommand command = new SqlCommand(Query, connection);
            connection.Open();
            int rowAffected = command.ExecuteNonQuery();           
            return rowAffected;

        }
       
    }
}
