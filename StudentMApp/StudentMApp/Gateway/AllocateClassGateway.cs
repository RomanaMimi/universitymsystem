﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using StudentMApp.Model;

namespace StudentMApp.Gateway
{
    public class AllocateClassGateway
    {
        public string ConnectionString = WebConfigurationManager.ConnectionStrings["SMString"].ConnectionString;

        public int AllocateClass(AllocateClass ac)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string query = "INSERT INTO AllocateClassroom(DeptId, CourseId, RoomNo, DayName, StartTime, EndTime, Classroomflag) VALUES('" + ac.DeptId + "', '" + ac.CourseId + "', '" + ac.RoomNo + "', '" + ac.DayName + "', '" + ac.StartTime + "', '"+ac.EndTime+"', '"+ac.Classflag+"')";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();
            int row = command.ExecuteNonQuery();
            connection.Close();
            return row;
        }

        public bool IsRoomAllocate(AllocateClass ac)
        {
            bool possible = false;
             //calculate current time
            TimeSpan STime = CalculateStartTime(ac.StartTime);
            TimeSpan ETime = CalculateStartTime(ac.EndTime);

            List<AllocateClass> classList = GetAllocateClass(ac);
            if (classList.Count == 0)
            {
                possible = true;
                return possible;
            }
            else
            {
                foreach (AllocateClass allocate in classList)
                {
                    TimeSpan startTime = CalculateStartTime(allocate.StartTime.Substring(0, 5));
                    TimeSpan endTime = CalculateEndTime(allocate.EndTime.Substring(0, 5));

                    string sFM = allocate.StartTime.Substring(6, 2);
                    string eFM = allocate.EndTime.Substring(6, 2);

                    if (ac.DayName == allocate.DayName)
                    {
                        if (ac.RoomNo == allocate.RoomNo)
                        {
                            if ((ac.StartTimeFormat == sFM && ac.EndTimeFormat == eFM))
                            {
                                if ((STime >= startTime && STime <= endTime) || (ETime >= startTime && ETime <= endTime))
                                {
                                    possible = false;
                                    break;
                                }
                                else
                                {
                                    possible = true;
                                    break;
                                }

                            }
                            else if (ac.StartTimeFormat == "AM" && ac.EndTimeFormat == "PM")
                            {
                                if ((ETime >= startTime && ETime >= endTime) || (ETime >= startTime && ETime <= endTime))
                                {
                                    possible = false;
                                    break;
                                }
                                else //(STime >= endTime )
                                {
                                    possible = true;
                                    break;
                                }
                            }
                            else if (ac.StartTimeFormat == "PM" && ac.EndTimeFormat == "AM")
                            {
                                 possible = false;
                                 break;
                            }
                            else
                            {
                                possible = true;
                                break;
                            }
                        }
                        else
                        {
                            possible = true;
                            break;
                        }

                    }
                }
            }

            
            return possible;

        }

        private List<AllocateClass> GetAllocateClass(AllocateClass ac)
        {
            List<AllocateClass> cList = new List<AllocateClass>();
            SqlConnection connection = new SqlConnection(ConnectionString);

            string query = "SELECT RoomNo, DayName, StartTime, EndTime FROM AllocateClassroom WHERE RoomNo='" + ac.RoomNo + "' AND DayName='"+ac.DayName+"'";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    AllocateClass c = new AllocateClass();
                    //c.CId = Convert.ToInt32(reader["Id"]);
                    c.RoomNo = reader["RoomNo"].ToString();
                    c.DayName = reader["DayName"].ToString();
                    c.StartTime = reader["StartTime"].ToString();
                    c.EndTime = reader["EndTime"].ToString();
                    cList.Add(c);
                }
            }
            reader.Close();
            connection.Close();

            return cList;
        }


        private TimeSpan CalculateStartTime(string time)
        {
            int hour = Convert.ToInt32(time.Substring(0, 2));
            int minute = Convert.ToInt32(time.Substring(3, 2));
            TimeSpan startTime = new TimeSpan(hour, minute, 0);
            return startTime;
        }

        private TimeSpan CalculateEndTime(string time)
        {
            
            int hour = Convert.ToInt32(time.Substring(0, 2));
            var minute = Convert.ToInt32(time.Substring(3, 2));
            TimeSpan endTime = new TimeSpan(hour, minute, 0);
            return endTime;
        }

        private int UpdateClassStatus(ClassSchedule c)
        {
            //Connection
            SqlConnection connection = new SqlConnection(ConnectionString);
            //Query
            string Query = "UPDATE ClassSchedule SET Schedule='" + c.Schedule + "',ClassFlag='" + c.ClassRoomFlag + "' WHERE ClassFlag=1";
            //Execute
            SqlCommand command = new SqlCommand(Query, connection);
            connection.Open();
            int rowAffected = command.ExecuteNonQuery();
            return rowAffected;

        }


        public int UpdateClassFlagStatus(ClassSchedule c)
        {
            //Connection
            SqlConnection connection = new SqlConnection(ConnectionString);
            //Query
            string Query = "UPDATE Course SET ClassFlag='" + c.ClassRoomFlag + "' WHERE ClassFlag=1";
            //Execute
            SqlCommand command = new SqlCommand(Query, connection);
            connection.Open();
            int rowAffected = command.ExecuteNonQuery();
            return rowAffected;

        }


        public int UnalloClassroom(AllocateClass c)
        {
            //Connection
            SqlConnection connection = new SqlConnection(ConnectionString);
            //Query
            string Query = "UPDATE AllocateClassroom SET Classroomflag='" + c.Classflag + "' WHERE Classroomflag=1";
            //Execute
            SqlCommand command = new SqlCommand(Query, connection);
            connection.Open();
            int rowAffected = command.ExecuteNonQuery();
            return rowAffected;
        }

        public List<AllocateClass> GetAllocateClasses()
        {
            List<AllocateClass> cList = new List<AllocateClass>();
            SqlConnection connection = new SqlConnection(ConnectionString);

            string query = "SELECT RoomNo, DayName, StartTime, EndTime, Classroomflag FROM AllocateClassroom WHERE Classroomflag=1";
            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    AllocateClass c = new AllocateClass();
                    //c.CId = Convert.ToInt32(reader["Id"]);
                    c.RoomNo = reader["RoomNo"].ToString();
                    c.DayName = reader["DayName"].ToString();
                    c.StartTime = reader["StartTime"].ToString();
                    c.EndTime = reader["EndTime"].ToString();
                    c.Classflag = Convert.ToInt32(reader["Classroomflag"]);
                    cList.Add(c);
                }
            }
            reader.Close();
            connection.Close();

            return cList;
        }
    }
}