﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using StudentMApp.Model;

namespace StudentMApp.BLL
{
    public class SemesterGateway
    {
        public string ConnectionString = WebConfigurationManager.ConnectionStrings["SMString"].ConnectionString;

        public List<Semester> GetAllInfo()
        {
            List<Semester> sList = new List<Semester>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            //query
            string query = "SELECT * FROM Semester";
            //execute
            SqlCommand command = new SqlCommand(query, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Semester sm = new Semester();
                    sm.SId = Convert.ToInt32(reader["Id"]);
                    sm.SName = reader["Name"].ToString();
                    sList.Add(sm);

                }
            }
            reader.Close();
            connection.Close();

            return sList;
        } 

    }
}