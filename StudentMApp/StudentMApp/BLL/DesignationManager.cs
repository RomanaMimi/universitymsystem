﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StudentMApp.Gateway;
using StudentMApp.Model;

namespace StudentMApp.BLL
{
    public class DesignationManager
    {
        DesignationGateway gateway = new DesignationGateway();

        public List<Designation> GetAllInfo()
        {
            return gateway.GetAllInfo();
        }
    }
}