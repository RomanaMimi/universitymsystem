﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StudentMApp.Gateway;
using StudentMApp.Model;

namespace StudentMApp.BLL
{
    public class DeptManager
    {
        DeptGateway gateway = new DeptGateway();

        public string Save(Department dept)
        {

            if (IsExist(dept.Code, dept.Name))
            {
                return "Code or Name must be unique";
            }
            if (dept.Code.Length < 2 || dept.Code.Length > 7)
            {
                return "Code length must be 2-7 characters long";
            }
            if (gateway.Save(dept) > 0)
            {
                return "Saved";
            }
            else
            {
                return "Not saved";
            }

            
        }

        public bool IsExist(string code, string name)
        {
            return gateway.IsExist(code, name);
        }


        public List<Department> GetAllDept()
        {
            return gateway.GetAllDept();
        }

    }
}