﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StudentMApp.Gateway;
using StudentMApp.Model;

namespace StudentMApp.BLL
{
    public class ClassScheduleManager
    {
        ClassScheduleGateway gateway = new ClassScheduleGateway();

        public int SaveSchedule(ClassSchedule cs)
        {
            int rowAffect = 0;
            if (isExist(cs) != null)
            {
                ClassSchedule cs2 = isExist(cs);
                cs2.Schedule = cs2.Schedule +";\r\n"+ cs.Schedule;
                cs2.ClassRoomFlag = 1;
                rowAffect = gateway.UpdateSchedule(cs2);
                return rowAffect;
            }
            rowAffect = gateway.SaveSchedule(cs);
            return rowAffect;   
        }

        public ClassSchedule isExist(ClassSchedule cs)
        {
            ClassSchedule cs1 = gateway.GetScheduleCourses().FirstOrDefault(p => p.CourseId == cs.CourseId);
            return cs1;
        }

        public int UpdateSchedule(ClassSchedule ac)
        {
            return gateway.UpdateSchedule(ac);
        }

        public List<ClassSchedule> GetScheduleCourses()
        {
            return gateway.GetScheduleCourses();
        }
    }
}