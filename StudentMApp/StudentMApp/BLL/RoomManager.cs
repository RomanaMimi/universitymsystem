﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StudentMApp.Gateway;
using StudentMApp.Model;

namespace StudentMApp.BLL
{
    public class RoomManager
    {
        RoomGateway gateway = new RoomGateway();

        public List<Room> GetRooms()
        {
            return gateway.GetRooms();
        }
    }
}