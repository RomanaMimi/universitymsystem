﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StudentMApp.Gateway;
using StudentMApp.Model;

namespace StudentMApp.BLL
{
    public class EnrollStudentManager
    {
        EnrollStudentGateway gateway = new EnrollStudentGateway();

        public string EnrollCourseToStudent(EnrollStudent enroll)
	    {
		    if(IsExist(enroll))
		    {
		        return "this course has already taken.";
		    }
		    if(gateway.EnrollCourseToStudent(enroll) > 0)
		    {
			    return "Saved";
		    }
		    return "not saved";
	    }


        private bool IsExist(EnrollStudent enroll)
        {
            return gateway.IsExist(enroll);
        }


        public List<Course> GetStudentCourse(int studentId)
        {
            return gateway.GetStudentCourse(studentId);
        }

        public string SaveResultToStudent(StudentResult student)
	    {
		    if(IsResultExist(student))
		    {
		        return "this course's result has already taken.";
		    }
		    if(gateway.SaveResultToStudent(student) > 0)
		    {
			    return "Saved";
		    }
		    return "not saved";
	    }



        private bool IsResultExist(StudentResult student)
        {
            return gateway.IsResultExist(student);
        }



       // to find result
        public List<StudentResult> GetStudentResult()
        {
            return gateway.GetStudentResult();
        }

    }
}