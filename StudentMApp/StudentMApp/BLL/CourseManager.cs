﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StudentMApp.Gateway;
using StudentMApp.Model;

namespace StudentMApp.BLL
{
    public class CourseManager
    {
        CourseGateway gateway = new CourseGateway();

        public string SaveCourse(Course crs)
        {
            if (IsExist(crs.CourseCode, crs.CourseName))
            {
                return "Code or Name must be unique";
            }
            if (crs.CourseCode.Length < 5)
            {
                return "Code length must be al least 5 characters long";
            }
            if (crs.CourseCredit < 0.5 && crs.CourseCredit > 5.0)
            {
                return "Credit range must be 0.5 to 5.0";
            }
            if (gateway.SaveCourse(crs) > 0)
            {
                return "Saved";
            }
            else
            {
                return "not saved";
            }
        }



        public bool IsExist(string code, string name)
        {
            return gateway.IsExist(code, name);
        }

        public bool IsCourseAssign(int teacherId, int courseId)
        {
            return gateway.IsCourseAssign(teacherId, courseId);
        }


        public List<Course> GetAllCourses()
        {
            return gateway.GetAllCourses();
        }

        public int UpdateCourseStatus(Course course)
        {
            return gateway.UpdateCourseStatus(course);
        }


        public int UpdateClassStatus(Course course)
        {
            return gateway.UpdateClassStatus(course);
        }

        public int UpdateCourseFlagStatus(Course c)
        {
            return gateway.UpdateCourseFlagStatus(c);
        }

        public int UpdateClassFlagStatus(Course c)
        {
            return gateway.UpdateClassFlagStatus(c);
        }

        public int UnassignCourses(Course course)
        {
            return gateway.UnassignCourses(course);
        }
    }
}
