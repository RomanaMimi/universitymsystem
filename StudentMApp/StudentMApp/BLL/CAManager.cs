﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StudentMApp.Gateway;
using StudentMApp.Model;

namespace StudentMApp.BLL
{
    public class CAManager
    {
        CAGateway gateway = new CAGateway();        
        public string AssignCourse(CourseAssign ca)
        {

            //if (IsExist(ca.TeacherId, ca.CourseId))
            //{
            //    return "exist";
            //}

            if (gateway.AssignCourse(ca) > 0)
            {
                return "Assign";
            }
            else
            {
                return "not assign";
            }         
        }

        public List<CourseAssign> GetAllCourses()
        {
            return gateway.GetAllCourses();
        }


        //public bool IsExist(int TId, int CId)
        //{
        //    return gateway.IsExist(TId, CId);
        //}

        public int UpdateCourseStatus(CourseAssign ca)
        {
            return gateway.UpdateCourseStatus(ca);
        }

    }
}