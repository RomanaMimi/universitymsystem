﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StudentMApp.Gateway;
using StudentMApp.Model;

namespace StudentMApp.BLL
{
    public class AllocateClassManager
    {
        AllocateClassGateway gateway = new AllocateClassGateway();

        public string AllocateClass(AllocateClass ac)
        {
            if (IsRoomAllocate(ac) == false)
            {
                return "this room has already allocated.";
            }
            else
            {
                ac.Classflag = 1;
                if (gateway.AllocateClass(ac) > 0)
                {
                    return "saved";
                }
            }  
            return "Error! not allocated";
        }

        public bool IsRoomAllocate(AllocateClass ac)
        {
            return gateway.IsRoomAllocate(ac);
        }

        public int UnalloClassroom(AllocateClass c)
        {
            return gateway.UnalloClassroom(c);
        }

        public List<AllocateClass> GetAllocateClasses()
        {
            return gateway.GetAllocateClasses();
        }
    }
}