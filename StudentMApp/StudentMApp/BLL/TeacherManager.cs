﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StudentMApp.Gateway;
using StudentMApp.Model;

namespace StudentMApp.BLL
{
    public class TeacherManager
    {
        TeacherGateway gateway = new TeacherGateway();

        public string SaveTeacher(Teacher teacher)
        {
            if (IsExist(teacher.Email))
            {
                return "Email must be unique";
            }
           
            if (teacher.Credit < 0)
            {
                return "Credit must be non-negative number";
            }
            if (gateway.SaveTeacher(teacher) > 0)
            {
                return "Saved";
            }
            else
            {
                return "not saved";
            }
        }


        public bool IsExist(string email)
        {
            return gateway.IsExist(email);
        }


        public List<Teacher> GetAllTeachers()
        {
            return gateway.GetAllTeachers();
        }

        public int UpdateTeacherStatus(Teacher teacher)
        {
            return gateway.UpdateTeacherStatus(teacher);
        }
    }
}
