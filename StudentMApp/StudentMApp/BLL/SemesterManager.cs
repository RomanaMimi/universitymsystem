﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StudentMApp.Model;

namespace StudentMApp.BLL
{
    public class SemesterManager
    {
        SemesterGateway gateway = new SemesterGateway();
        public List<Semester> GetAllInfo()
        {
            return gateway.GetAllInfo();
        }

    }
}