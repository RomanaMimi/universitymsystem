﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StudentMApp.Gateway;
using StudentMApp.Model;

namespace StudentMApp.BLL
{
    public class StudentManager
    {
        StudentGateway gateway = new StudentGateway();
	
	public string SaveStudent(Student student)
	{
		if(IsExist(student.Email))
		{
			return "this student already existed.";
		}
		if(gateway.SaveStudent(student) > 0)
		{
			return "Saved";
		}
		return "not saved";
	}

	private bool IsExist(string email)
	{
		return gateway.IsExist(email);
	}

	public List<Student> CountStudent(int deptId, string year)
	{
		return gateway.CountStudent(deptId, year);
	}

	
	public Student GetNewStudent(string NewRegNo)
	{
		return gateway.GetNewStudent(NewRegNo);
	}





        public List<Student> GetAllRegNo()
        {
            return gateway.GetAllRegNo();
        }


        public List<Student> GetAllStudents()
        {
            return gateway.GetAllStudents();
        }

    }
}