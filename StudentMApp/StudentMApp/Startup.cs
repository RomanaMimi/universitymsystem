﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(StudentMApp.Startup))]
namespace StudentMApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
