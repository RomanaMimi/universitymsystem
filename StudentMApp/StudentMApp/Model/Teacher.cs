﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StudentMApp.Model
{
    public class Teacher
    {
        public int TId { get; set; }


        [Required(ErrorMessage = "Please enter name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter address")]
        public string Address { get; set; }


        [Required(ErrorMessage = "Please enter your email")]
        [RegularExpression(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", ErrorMessage = "Enter valid email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter address")]
        [DisplayName("Contact No")]
        public string Contact { get; set; }


        [Required(ErrorMessage = "Please select designation")]
        [DisplayName("Designation")]
        public int DesignID { get; set; }

        public string Designation { get; set; }

        [Required(ErrorMessage = "Please select department")]
        [DisplayName("Department")]
        public int DeptID { get; set; }

        public string Department { get; set; }

        [Required(ErrorMessage = "Please enter credit")]
        [DisplayName("Credit to be taken")]
        public float Credit { get; set; }

        public float TotalCredit { get; set; }
        public float DueCredit { get; set; }

    }
}