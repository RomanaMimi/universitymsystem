﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StudentMApp.Model
{
    public class Department
    {
        [Display(Name = "Dept Id")]
        public int Id { get; set; }

        [Display(Name = "Department Code")]
        [Required(ErrorMessage = "Please enter department code")]
        [StringLength(7, MinimumLength = 2, ErrorMessage = "Code must be 2 to 7 characters long.")]
        public string Code { get; set; }

        [Display(Name = "Department Name")]
        [Required(ErrorMessage = "Please enter department name")]
        public string Name { get; set; }
    }
}