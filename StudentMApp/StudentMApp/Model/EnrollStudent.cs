﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StudentMApp.Model
{
    public class EnrollStudent
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "please select Reg. No")]
        [DisplayName("Student Reg No.")]
        public int RegNo { get; set; }

        public string StudentRegNo { get; set; }

        [Required(ErrorMessage = "name required")]
        [DisplayName("Name")]
        public string StudentName { get; set; }

        [Required(ErrorMessage = "email required")]
        [DisplayName("Email")]
        public string StudentEmail { get; set; }

        [Required(ErrorMessage = "department required")]
        public string Department { get; set; }



        //[Required]
        public int deptId { get; set; }



        [Required(ErrorMessage = "please select course")]
        [DisplayName("Course")]
        public int courseId { get; set; }
        public string courseCode { get; set; }
        public string courseName { get; set; }


        [DisplayName("Date")]
        public string EnrollDate { get; set; }

        //[DisplayName("Select Grade Letter")]
        //public string Grade { get; set; }

    }
}