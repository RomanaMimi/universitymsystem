﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentMApp.Model
{
    public class ClassSchedule
    {
        public int Id { get; set; }

        public int CourseId { get; set; }
        public string CourseCode { get; set; }

        public string CourseName { get; set; }
        public string Schedule { get; set; }
        public int ClassRoomFlag { get; set; }
    }
}