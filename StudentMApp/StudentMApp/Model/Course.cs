﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StudentMApp.Model
{
    public class Course
    {
        [DisplayName("Course ID")]
        public int CId { get; set; }

        [Required(ErrorMessage = "Please enter course code")]
        [MinLength(5, ErrorMessage = "Code must be 5 characters long.")]
        [DisplayName("Course Code")]
        public string CourseCode { get; set; }


        [Required(ErrorMessage = "Please enter course name")]
        [DisplayName("Course Name")]
        public string CourseName { get; set; }

        [Required(ErrorMessage = "Please enter course credit")]
        [DisplayName("Credit")]
        public float CourseCredit { get; set; }


        [Required(ErrorMessage = "Please enter course description")]
        [DisplayName("Description")]
        public string CourseDes { get; set; }



        [Required(ErrorMessage = "Please select department")]
        [DisplayName("Department")]
        public int DeptId { get; set; }


        [Required(ErrorMessage = "Please select semester")]
        [DisplayName("Semester ID")]
        public int SemesterId { get; set; }

        [DisplayName("Semester")]
        public string SemName { get; set; }

        public int TeacherId { get; set; }

        [DisplayName("Assign To")]
        public string TeacherName { get; set; }

        public int CourseFlag { get; set; }
        public int Classflag { get; set; }

        public string Text { get; set; }
        public string Schedule { get; set; }
    }
}