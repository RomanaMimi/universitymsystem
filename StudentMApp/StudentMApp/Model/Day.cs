﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentMApp.Model
{
    public class Day
    {
        public int Id { get; set; }
        public string DayName { get; set; }

    }
}