﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentMApp.Model
{
    public class Semester
    {
        public int SId { get; set; }
        public string SName { get; set; }
    }
}