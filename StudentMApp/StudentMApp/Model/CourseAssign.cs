﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace StudentMApp.Model
{
    public class CourseAssign
    {
        public int Id { get; set; }


        [Required(ErrorMessage = "Please select department")]
        [DisplayName("Department")]
        public int DepartmentId { get; set; }


        [Required(ErrorMessage = "Please select teacher")]
        [DisplayName("Teacher")]

        public int TeacherId { get; set; }

        public float TotalCredit { get; set; } // get from teacher

        [Required(ErrorMessage = "Credit required")]
        [DisplayName("Credit to be taken")]
        public float CreditTaken { get; set; } // get from teacher

        [Required(ErrorMessage = "Due Credit required")]
        [DisplayName("Remaining Credit")]
        public float DueCredit { get; set; }


        [Required(ErrorMessage = "Please select course")]
        [DisplayName("Course")]
        public int CourseId { get; set; }

        [Required(ErrorMessage = "Course name required")]
        [DisplayName("Name")]
        public string CourseName { get; set; }

        [Required(ErrorMessage = "Course Credit required")]
        [DisplayName("Credit")]
        public float CourseCredit { get; set; }

        public string CourseSemester { get; set; }

        public int semesterID { get; set; } // get from manager

        public int Courseflag { get; set; }








    }
}