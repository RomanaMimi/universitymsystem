﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StudentMApp.Model
{
    public class AllocateClass
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "please select department")]
        [DisplayName("Department")]
        public int DeptId { get; set; }

        [Required(ErrorMessage = "please select course")]
        [DisplayName("Course")]
        public int CourseId { get; set; }

        [Required(ErrorMessage = "please select room")]
        [DisplayName("Room No")]
        public string RoomNo { get; set; }
      
        [Required(ErrorMessage = "please select day")]
        [DisplayName("Day")]
        public string DayName { get; set; }

        [Required(ErrorMessage = "please select start time")]
        [DisplayName("From")]
        public string StartTime { get; set; }

        //[Required(ErrorMessage = "please select time format")]
        public string StartTimeFormat { get; set; }

        [Required(ErrorMessage = "please select end time")]
        [DisplayName("To")]
        public string EndTime { get; set; }

        //[Required(ErrorMessage = "please select time format")]
        public string EndTimeFormat { get; set; }

        public int Classflag { get; set; }

        public string ClassSchedule { get; set; }
    }
}