﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentMApp.Model
{
    public class Grade
    {
        public int Id { get; set; }
        public string GradeName { get; set; }
    }
}