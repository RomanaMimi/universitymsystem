﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StudentMApp.Model
{
    public class Student
    {
        public int Id { get; set; }

        public string RegNo { get; set; }

        [Required(ErrorMessage = "please enter your name")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "at least 3 characters long ")]
        public string Name { get; set; }

        [Required(ErrorMessage = "please enter your email")]
        [RegularExpression(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", ErrorMessage = "enter valid email")]
        [StringLength(30)]
        public string Email { get; set; }

        [Required(ErrorMessage = "please enter contact number")]
        [StringLength(11, MinimumLength = 7, ErrorMessage = "must be 7 to 11 charchetrs long")]
        [DisplayName("Contact No")]
        public string ContactNo { get; set; }

        [Required(ErrorMessage = "please enter address")]
        public string Address { get; set; }

        [Required(ErrorMessage = "please select date")]
        [DisplayName("Date")]
        public string RegDate { get; set; }

        [Required(ErrorMessage = "please select department")]
        [DisplayName("Department")]
        public int DeptId { get; set; }
        
        public string Year { get; set; }

        public string DeptCode { get; set; }

        public string DeptName { get; set; }
    }
}