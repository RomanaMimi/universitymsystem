﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentMApp.Model
{
    public class Room
    {
        public int Id { get; set; }
        public string RoomNo { get; set; }
    }
}